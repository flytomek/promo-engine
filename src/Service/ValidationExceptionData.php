<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Validator\ConstraintViolationList;

class ValidationExceptionData extends ServiceExceptionData
{
    public function __construct(int $statusCode, string $type, private readonly ConstraintViolationList $violationList)
    {
        parent::__construct($statusCode, $type);
    }

    public function toArray(): array
    {
        return [
            'type' => 'ConstraintViolationList',
            'violations' => $this->getViolationsArray(),
        ];
    }

    public function getViolationsArray(): array
    {
        $violationsArray = [];

        foreach ($this->violationList as $violation) {
            $violationsArray[] = [
                'propertyPath' => $violation->getPropertyPath(),
                'message' => $violation->getMessage(),
            ];
        }

        return $violationsArray;
    }
}
