<?php

declare(strict_types=1);

namespace App\Filter\Modifier;

use App\DTO\PromotionEnquiryInterface;
use App\Entity\Promotion;

class DateRangeMultiplier implements PriceModifierInterface
{
    public function modify(int $price, int $quantity, Promotion $promotion, PromotionEnquiryInterface $enquiry): int
    {
        $requestDate = date_create_immutable($enquiry->getRequestDate());
        $from = date_create_immutable($promotion->getCriteria()['from']);
        $to = date_create_immutable($promotion->getCriteria()['to']);

        if (!($requestDate >= $from && $requestDate < $to)) {
            return $price * $quantity;
        }

        return (int) (($price * $quantity) * $promotion->getAdjustment());
    }
}
