<?php

declare(strict_types=1);

namespace App\Filter\Modifier;

use App\DTO\PromotionEnquiryInterface;
use App\Entity\Promotion;

class EvenItemsMultiplier implements PriceModifierInterface
{
    public function modify(int $price, int $quantity, Promotion $promotion, PromotionEnquiryInterface $enquiry): int
    {
        if ($quantity < $promotion->getCriteria()['minimum_quantity']) {
            return $price * $quantity;
        }

        // Odd
        if (1 === $quantity % 2) {
            return (int) ($price * ($quantity - 1) * $promotion->getAdjustment() + $price);
        }

        // Even
        return (int) ($price * $quantity * $promotion->getAdjustment());
    }
}
