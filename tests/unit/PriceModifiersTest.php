<?php

declare(strict_types=1);

namespace App\Tests\unit;

use App\DTO\LowestPriceEnquiry;
use App\Entity\Promotion;
use App\Filter\Modifier\DateRangeMultiplier;
use App\Filter\Modifier\EvenItemsMultiplier;
use App\Filter\Modifier\FixedPriceVoucher;
use App\Tests\ServiceTestCase;

class PriceModifiersTest extends ServiceTestCase
{
    /** @test */
    public function dateRangeMultiplierReturnsCorrectlyModifiedPrice(): void
    {
        // Given
        $promotion = new Promotion();
        $promotion->setName('Black Friday half price sale');
        $promotion->setAdjustment(0.5);
        $promotion->setCriteria(['from' => '2022-11-25', 'to' => '2022-11-28']);
        $promotion->setType('date_range_multiplier');

        $enquiry = new LowestPriceEnquiry();
        $enquiry->setRequestDate('2022-11-27');

        $dateRangeMultiplier = new DateRangeMultiplier();

        // When
        $modifiedPrice = $dateRangeMultiplier->modify(100, 5, $promotion, $enquiry);

        // Then
        $this->assertEquals(250, $modifiedPrice);
    }

    /** @test */
    public function fixedPriceVoucherReturnsCorrectlyModifiedPrice(): void
    {
        // Given
        $promotion = new Promotion();
        $promotion->setName('Voucher OU812');
        $promotion->setAdjustment(100);
        $promotion->setCriteria(['code' => 'OU812']);
        $promotion->setType('fixed_price_voucher');

        $enquiry = new LowestPriceEnquiry();
        $enquiry->setVoucherCode('OU812');

        $fixedPriceVoucher = new FixedPriceVoucher();

        // When

        $modifiedPrice = $fixedPriceVoucher->modify(100, 5, $promotion, $enquiry);

        // Then
        $this->assertEquals(400, $modifiedPrice);
    }

    /** @test */
    public function evenItemsMultiplierReturnsCorrectlyModifiedPrice(): void
    {
        // Given
        $promotion = new Promotion();
        $promotion->setName('Buy one get one free');
        $promotion->setAdjustment(0.5);
        $promotion->setCriteria(['minimum_quantity' => 2]);
        $promotion->setType('even_items_multiplier');

        $enquiry = new LowestPriceEnquiry();

        $evenItemsMultiplier = new EvenItemsMultiplier();

        // When

        $modifiedPrice = $evenItemsMultiplier->modify(100, 5, $promotion, $enquiry);

        // Then
        $this->assertEquals(300, $modifiedPrice);
    }

    /** @test */
    public function evenItemsMultiplierReturnsCorrectlyModifiedPriceForAlternativeCalculations(): void
    {
        // Given
        $promotion = new Promotion();
        $promotion->setName('Buy one get one half price');
        $promotion->setAdjustment(0.75);
        $promotion->setCriteria(['minimum_quantity' => 2]);
        $promotion->setType('even_items_multiplier');

        $enquiry = new LowestPriceEnquiry();

        $evenItemsMultiplier = new EvenItemsMultiplier();

        // When

        $modifiedPrice = $evenItemsMultiplier->modify(100, 5, $promotion, $enquiry);

        // Then
        $this->assertEquals(400, $modifiedPrice);
    }
}
